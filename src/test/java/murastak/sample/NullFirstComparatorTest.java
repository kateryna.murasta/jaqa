package murastak.sample;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NullFirstComparatorTest {
    @Test
    public void testSortList() {
        List<String> list = Arrays.asList(null, "foo", "bar", null);
        list.sort(new NullFirstComparator());
        assertEquals(Arrays.asList(null, null, "bar", "foo"), list);
    }

    @Test
    public void testReversedOrder() {
        List<String> list = Arrays.asList(null, "foo", "bar", null);
        list.sort(new NullFirstComparator().reversed());
        assertEquals(Arrays.asList("foo", "bar", null, null), list);
    }
}