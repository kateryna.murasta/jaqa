package murastak.sample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class GoodsSortingTest {
    private List<Product> store;

    @BeforeEach
    public void init() {
        store = new ArrayList<>();
        store.add(new Product("Apple", 1.2, "", LocalDate.now().plusDays(30), 500));
        store.add(new Product("Milk", 1.99, "", LocalDate.now().plusDays(10), 45));
        store.add(new Product("Bread", 1.01, "", LocalDate.now().plusDays(1), 30));
        store.add(new Product("Rise", 3.10, "", LocalDate.now().plusDays(150), 399));
        System.out.println(store);
    }

    @Test
    public void testSortingListByName() {
        Comparator<Product> productNameComparator = new Comparator<>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.name.compareTo(o2.name);
            }
        };
        store.sort(productNameComparator);
        System.out.println(store);
    }

    @Test
    public void testSortingListByPrice() {
        Comparator<Product> productPriceComparator = new Comparator<>() {
            @Override
            public int compare(Product o1, Product o2) {
                return Double.compare(o1.price, o2.price);
            }
        };
        store.sort(productPriceComparator);
        System.out.println(store);
    }

    @Test
    public void testSortingListByProducer() {
        Comparator<Product> productProducerComparator = new Comparator<>() {
            @Override
            public int compare(Product o1, Product o2) {
                return o1.producer.compareTo(o2.producer);
            }
        };
        store.sort(productProducerComparator);
        System.out.println(store);
    }

    @Test
    public void testSortingListByQuantity() {
        Comparator<Product> productQuantityComparator = new Comparator<>() {
            @Override
            public int compare(Product o1, Product o2) {
                return Integer.compare(o1.getQuantity(), o2.getQuantity());
            }
        };
        store.sort(productQuantityComparator);
        System.out.println(store);

        Product productToSell = store.get(store.size() - 1);
        productToSell.sell(productToSell.getQuantity());

        store.sort(productQuantityComparator);
        System.out.println(store);
    }
}
