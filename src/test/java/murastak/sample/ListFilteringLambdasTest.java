package murastak.sample;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ListFilteringLambdasTest {
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    IntegerTester integerTester = new IntegerTester();

    @Test
    public void testPrintOnlyFive () {
        System.out.print("Only Fives: ");
        Predicate<Integer> filter = (Integer n) -> {return n == 5;};
        // Predicate<Integer> filter = (n) -> n == 5;
        // Predicate<Integer> filter = n -> n == 5;
        integerTester.evaluate(list, filter);
        //integerTester.evaluate(list, n -> n == 5);
    }

    @Test
    public void testPrintAll()  {
        System.out.print("All: ");
        integerTester.evaluate(list, n -> true);
    }

    @Test
    public void testPrintNone()  {
        System.out.print("None: ");
        integerTester.evaluate(list, n -> false);
    }

    @Test
    public void testPrintEven()  {
        System.out.print("Evens: ");
        integerTester.evaluate(list, n -> n%2 == 0 );
    }

    @Test
    public void testPrintOdd()  {
        System.out.print("Odds: ");
        integerTester.evaluate(list, n -> n%2 == 1 );
    }

    @Test
    public void testPrintGraterThanFive()  {
        System.out.print("Grater than 5: ");
        integerTester.evaluate(list, n -> n > 5 );
    }
}
