package murastak.sample;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ListFilteringClassTest {
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    IntegerTester integerTester = new IntegerTester();

    static class FiveFilter implements Predicate<Integer>{
        @Override
        public boolean test(Integer n) {
            return n == 5;
        }
    }

    @Test
    public void testPrintOnlyFive () {
        System.out.print("Only Fives: ");
        FiveFilter filter = new FiveFilter();
        integerTester.evaluate(list, filter);
    }
}
