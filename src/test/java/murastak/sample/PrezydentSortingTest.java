package murastak.sample;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PrezydentSortingTest {
    List<Prezydent> list;

    @BeforeEach
    public void init() {
        list = new ArrayList<>();
        list.add(new Prezydent("Jacek Majchrowski", "Sosnowiec"));
        list.add(new Prezydent("Andrzej Gołaś", "Kraków"));
        list.add(new Prezydent("Józef Lassota", "Siedliska"));
        list.add(new Prezydent("Krzysztof Bachmiński", "Zabrze"));
        System.out.println(list);
    }

    @Test
    public void testSortingByName() {
        list.sort(new CustomerNameComparator());
        System.out.println(list);
    }

    @Test
    public void testSortingByAddress() {
        list.sort(new CustomerAddressComparator());
        System.out.println(list);
    }

    @Test
    public void testSortingById() {
        list.sort(new CustomerIdComparator());
        System.out.println(list);
    }
}
