package murastak.sample;

import org.junit.jupiter.api.Test;

public class ThreadTest {
    @Test
    public void testThreading() throws InterruptedException {
        AnotherTask thread = new AnotherTask();
        thread.start();

        Thread anonymousThread = new Thread(){
            @Override
            public void run() {
                for (int i = 0; i < 5; i++) {
                    System.out.println("anonymousClassThread" + i);
                    try {
                        Thread.sleep(10000);
                    }
                    catch (InterruptedException e) {
                        System.out.println("Interrupt");
                    }
                }
            }
        };
        anonymousThread.start();

        Thread lambdaThread = new Thread(() -> {
            for (int i = 0; i < 3; i++) {
                System.out.println("Lambda Thread" + i);
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        lambdaThread.start();

        for (int i = 0; i < 3; i++) {
            System.out.println("Main Thread" + i);
            Thread.sleep(10000);
        }

        thread.join();
        anonymousThread.join();
        lambdaThread.join();

        System.out.println("End");
    }
}

class AnotherTask extends Thread {
    @Override
    public void run(){
        for (int i = 0; i < 5; i++) {
            System.out.println("AnotherTask" + i);
            try {
                Thread.sleep(1000);
            }
            catch (InterruptedException e) {
                System.out.println("Interrupt");
            }
        }
    }
}