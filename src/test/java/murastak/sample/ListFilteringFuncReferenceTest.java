package murastak.sample;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class ListFilteringFuncReferenceTest {
    List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
    IntegerTester integerTester = new IntegerTester();

    public static boolean testNumberEqToFive(Integer n){
        return n == 5;
    }

    @Test
    public void testPrintOnlyFive () {
        System.out.print("Only Fives: ");
        integerTester.evaluate(list, ListFilteringFuncReferenceTest::testNumberEqToFive);
    }
}
