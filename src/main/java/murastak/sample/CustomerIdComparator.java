package murastak.sample;

import java.util.Comparator;

public class CustomerIdComparator implements Comparator<Prezydent> {
    @Override
    public int compare(Prezydent o1, Prezydent o2) {
        return Integer.compare(o1.hashCode(), o2.hashCode());
    }
}
