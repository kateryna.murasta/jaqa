package murastak.sample;

import java.util.Comparator;

public class CustomerAddressComparator implements Comparator<Prezydent> {

    @Override
    public int compare(Prezydent o1, Prezydent o2) {
        return o1.address.compareTo(o2.address);
    }
}
