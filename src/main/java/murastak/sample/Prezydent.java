package murastak.sample;

public class Prezydent {
    public final String name;
    public final String address;

    public Prezydent(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return String.format("[%s (%s): %s]\n", name, hashCode(), address);
    }
}
