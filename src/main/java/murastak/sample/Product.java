package murastak.sample;

import java.time.LocalDate;

public class Product {
    public final String name;
    public final double price;
    public final String producer;
    public final LocalDate bestBefore;
    private int quantity;

    public Product(String name, double price, String producer, LocalDate bestBefore, int quantity) {
        this.name = name;
        this.price = price;
        this.producer = producer;
        this.bestBefore = bestBefore;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void sell(int amount) {
        quantity -= amount;
    }

    public void stock(int amount) {
        quantity += amount;
    }

    @Override
    public String toString() {
        return String.format("[%s (%s): price - %s, q-ty - %s, best before - %s]\n", name, producer, price, quantity, bestBefore);
    }
}
