package murastak.sample;

import java.util.Comparator;

public class CustomerNameComparator implements Comparator<Prezydent> {
    @Override
    public int compare(Prezydent o1, Prezydent o2) {
        return o1.name.compareTo(o2.name);
    }
}
