package murastak.sample;

import java.util.List;
import java.util.function.Predicate;

public class IntegerTester {
    public void evaluate(List<Integer> list, Predicate<Integer> predicate) {
        for(Integer n: list)  {
            if(predicate.test(n)) {
                System.out.print(n + " ");
            }
        }
        System.out.println();
    }
}
